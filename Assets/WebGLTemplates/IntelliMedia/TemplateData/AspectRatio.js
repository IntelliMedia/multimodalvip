function getRatio()
{
  var width = window.innerWidth;
  var height = window.innerHeight;

  var factor = Math.min(Math.floor(height / 9), Math.floor(width / 16));

  var maxWidth = 16 * factor;
  var maxHeight = 9 * factor;

  document.getElementById("aspect").style.width = maxWidth + "px";
  document.getElementById("aspect").style.height = maxHeight + "px";

  document.getElementById("#canvas").style.width = maxWidth + "px";
  document.getElementById("#canvas").style.height = maxHeight + "px";
  
}

window.onload = getRatio;
window.onresize = getRatio;