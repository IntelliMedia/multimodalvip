var loadingBox = null;
var progressBar = null;
var canvas = null;

function LoadTest() {
  var game = {
    Module: {},
    container: document.getElementById("gameContainer")
  };
  var canvas = document.createElement("canvas");
  game.container.appendChild(canvas);
  UnityProgress(game, 0.65);
}

function UnityProgress(gameInstance, progress) {
  if (!gameInstance.Module) {
    return;
  }

  if (!loadingBox) {
    loadingBox = document.getElementById("loadingBox");
    progressBar = document.getElementById("progressBar");

    canvas = gameInstance.container.firstChild;
    gameInstance.container.insertBefore(loadingBox, canvas);
    canvas.style.display = "none";
    loadingBox.style.display = "block";
  }

  if (loadingBox) {
    var length = 100 * Math.min(progress, 1);
    progressBar.style.width = length + "%";
    if (progress == 1) {
      loadingBox.remove();
      canvas.style.display = "block";
    }
  }
}
