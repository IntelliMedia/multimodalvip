﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntelliMediaCore.Unity.Utilities;

public class VIPInterface : MonoBehaviour
{
    public Button startKinect;
    public Button stopKinect;
    public Button startOpenFace;
    public Button stopOpenFace;
    public Button startOpenSmile;
    public Button stopOpenSmile;
    public Button startAudioRecord;
    public Button stopAudioRecord;
    public Button connectToServer;
    public Button disconnectFromServer;
    public Button startServer;
    public InputField usernames;
    public InputField numPlayers;
    //public InputField port;
    //public InputField ip;
    //public InputField traceLogBackupDrive;
    //public InputField multimodalServerExe;
    private AudioRecorder audioRecorder;

    void Start()
    {
        connectToServer.onClick.AddListener(delegate {
            SetupServer();
            _ = MultimodalServerInterface.StartServer();
            MultimodalServerInterface.ConnectToServer();
        });
        disconnectFromServer.onClick.AddListener(delegate {
            MultimodalServerInterface.DisconnectFromServer();
        });
        startKinect.onClick.AddListener(delegate {
            SetupServer();
            MultimodalServerInterface.StartKinectLogging();
        });
        stopKinect.onClick.AddListener(delegate {
            MultimodalServerInterface.StopKinectLogging();
        });
        startOpenFace.onClick.AddListener(delegate {
            SetupServer();
            MultimodalServerInterface.StartOpenFaceLogging();
        });
        stopOpenFace.onClick.AddListener(delegate {
            MultimodalServerInterface.StopOpenFaceLogging();
        });
        startOpenSmile.onClick.AddListener(delegate {
            SetupServer();
            MultimodalServerInterface.StartOpenSmileLogging();
        });
        stopOpenSmile.onClick.AddListener(delegate {
            MultimodalServerInterface.StopOpenSmileLogging();
        });
        startAudioRecord.onClick.AddListener(delegate {
            audioRecorder = new AudioRecorder(null, null);
            audioRecorder.StartRecording();
        });
        stopAudioRecord.onClick.AddListener(delegate {
            audioRecorder.StopRecording();
            audioRecorder = null;
        });
        //startServer.onClick.AddListener(delegate {
        //    SetupServer();
        //    _ = MultimodalServerInterface.StartServer();
        //});
    }

    void SetupServer()
    {
        MultimodalServerInterface.Instance.Usernames = usernames.text;
        int.TryParse(numPlayers.text, out MultimodalServerInterface.Instance.numPlayers);
        //MultimodalServerInterface.Instance.port = port.text;
        //MultimodalServerInterface.Instance.ip = ip.text;
        //MultimodalServerInterface.Instance.traceLogBackupDrive = traceLogBackupDrive.text;
        //MultimodalServerInterface.Instance.multimodalServerExeName = multimodalServerExe.text;
        MultimodalServerInterface.Instance.port = "8181";
        MultimodalServerInterface.Instance.ip = "127.0.0.1";
    }
}
