﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class AudioRecorder
{
    private AudioClip myAudioClip;
    private string myAudioOutPath;
    private string myAudioDeviceName;
    private string myAudioFileName;

	// Set default recording time to 1800s (30m)
	// Recording continue upto 1800s unless StopRecording method 
	//   is called to terminate recording
	public int myRecordingTime = 1800;

	public AudioRecorder(string deviceName, string outPath)
	{
		myAudioDeviceName = deviceName;
        myAudioOutPath = outPath;
        myAudioFileName = null;
        myAudioClip = null;
	}

	public void StartRecording()
	{
		try
		{
            // StartRecording
            // check to see if we can detect any microphone
            if (Microphone.devices.Length > 0)
			{
                if (!Microphone.IsRecording(myAudioDeviceName))
				{
                    Debug.Log("Start audio recording");
                    myAudioClip = Microphone.Start(null, false, myRecordingTime, 44100);
				}
				else
				{
					Debug.LogWarning("Failed to start recording: Current microphone is already occupied");
				}
			}
			else
			{
				Debug.LogWarning("No Microphone is detected");
			}
		}
		catch (Exception e)
		{
			Debug.LogError(e.Message);
			throw;
		}
	}

    // Stops recording
    // Wave file is saved at
    //     C:\Users\[userid]\AppData\LocalLow\IntelliMedia\MultimodalVIP\Audio\
    // Wave file name format is
    //     yyMMdd-HHmmss-fff 
    public void StopRecording()
    {
        try
        {
            // StopRecording
            // get last recorded time position
            int lastTime = Microphone.GetPosition(myAudioDeviceName);

            Microphone.End("");
            Debug.Log("Stop audio recording");

            // SaveRecording
            // get all audio sample data
            float[] samples = new float[myAudioClip.samples];
            myAudioClip.GetData(samples, 0);

            // create a place where we can store new audio sample
            float[] clipSamples = new float[lastTime];

            // Copy audio samples only upto where it stopped
            Array.Copy(samples, clipSamples, clipSamples.Length - 1);
            AudioClip newClip = AudioClip.Create("myNewClip", clipSamples.Length, 1, 44100, false);
            newClip.SetData(clipSamples, 0);

            string filePath;
            Debug.Log("Save audio recording");

            // Save audio in specified directory path
            WavUtility.FromAudioClip(newClip, out filePath, true, "Audio");
            Debug.Log(filePath);
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            throw;
        }
    }

    public void SetDeviceName(string deviceName)
    {
        myAudioDeviceName = deviceName;
    }

    public void SetOutPath(string outPath)
    {
        myAudioOutPath = outPath;
    }

    public void SetFileName(string fileName)
    {
        myAudioFileName = fileName;
    }
}
