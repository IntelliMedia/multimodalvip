﻿#if !UNITY_WEBGL
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using WebSocketSharp;
using IntelliMediaCore.Common.DataModels;
using IntelliMediaCore.Common.IoC;
using IntelliMediaCore.Common.Services;
using IntelliMediaCore.Common.Utilities;
using IntelliMediaCore.Unity.Services;
using Multimodal;
using Newtonsoft.Json;
using Parse.Common.Internal;
using IntelliMediaCore.Unity.Utilities;

public class MultimodalServerInterface : MonoBehaviourSingleton<MultimodalServerInterface>
{
    public static bool IsConnected => Instance.multimodalServerProcess != null && Instance.serverSocket != null;

    public int connectAttempts;
    public int retryTime;
    //public string multimodalServerExeName;
    public string userId = "";
    public int numPlayers;
    //public string traceLogBackupDrive;
    public string ip = "127.0.0.1";
    public string port = "8181";
    private Process multimodalServerProcess;
    private WebSocket serverSocket;

    private float logStartTime;

    private string usernames;

    public string Usernames
    {
        get
        {
            if (string.IsNullOrEmpty(usernames))
            {
                for (var a = 0 ; a < numPlayers ; ++a)
                {
                    var id = string.IsNullOrEmpty(userId) ? Guid.NewGuid().ToString() : userId;

                    usernames += "user_" + id;

                    if (a + 1 != numPlayers)
                    {
                        usernames += "_";
                    }
                }
            }

            return usernames;
        }
        set => usernames = value;
    }


    /// <summary>
    /// Attempt to start the multimodal server.
    /// </summary>
    public static async Task StartServer()
    {
        /*
        if (Instance.connectAttempts == -2)
        {
            Instance.connectAttempts = 3;
        }
        else if (Instance.connectAttempts == 0)
        {
            Instance.connectAttempts = -2;
            return;
        }

        Instance.connectAttempts--;
        */

        //if (!string.IsNullOrEmpty(Instance.multimodalServerExeName))
        //{
        //    var path = Instance.multimodalServerExeName;

        //    try
        //    {
        //        Instance.multimodalServerProcess = Process.GetProcessesByName("MultimodalServer")[0];
        //        /*
        //        Instance.multimodalServerProcess = new Process();
        //        Instance.multimodalServerProcess.StartInfo.FileName = path;
        //        Instance.multimodalServerProcess.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;

        //        if (!Instance.multimodalServerProcess.Start())
        //        {
        //            throw new Win32Exception("Sensor process couldn't start");
        //        }
        //        */

        //        // Instance.multimodalServerProcess = Process.GetProcessesByName(Instance.service.CurrentActivity.Config.MultimodalServerExeName)[0];
        //    }
        //    catch (Win32Exception sensor)
        //    {
        //        DebugLog.Error(sensor + ": Sensors are not ready.");

        //        Instance.multimodalServerProcess = null;

        //        // Attempt to reconnect.
        //        await Task.Delay(Instance.retryTime);
        //        await StartServer();
        //    }
        //    catch (Exception)
        //    {
        //        DebugLog.Error("Could not start Kinect sensor.");
        //    }
        //}
        //else
        //{
        //    DebugLog.Error("Path to multimodal server not specified");
        //}

        try
        {
            Instance.multimodalServerProcess = Process.GetProcessesByName("MultimodalServer")[0];
        }
        catch (Exception)
        {
            Instance.multimodalServerProcess = null;

            DebugLog.Error("Could not find a running multimodal server. Please execute the multimodalserver.exe first.");
        }
    }

    /// <summary>
    /// Stops the multimodal server.
    /// </summary>
    public static void StopServer()
    {
        DebugLog.Info("About to stop the multimodal server");
        Instance.multimodalServerProcess?.Kill();
    }

    /// <summary>
    /// Send a message to the server to start logging for Kinect and Open Face.
    /// </summary>
    public static void StartAllLogging()
    {
        if (Instance.multimodalServerProcess == null)
        {
            DebugLog.Error("Cannot start logging: multimodal server not detected as started.");

            return;
        }

        if (Instance.serverSocket == null)
        {
            ConnectToServer();
        }

        StartKinectLogging();
        StartOpenFaceLogging();
        StartOpenSmileLogging();
        //StartTraceLog();
    }

    /// <summary>
    /// Send a message to the server to stop logging for Kinect and Open Face.
    /// </summary>
    public static void StopAllLogging()
    {
        StopKinectLogging();
        StopOpenFaceLogging();
        StopOpenSmileLogging();
        //StopTraceLog();

        Instance.Usernames = "";
    }

    /// <summary>
    /// Send a message to the server to start the Kinect logging.
    /// </summary>
    public static void StartKinectLogging()
    {
        if (Instance.multimodalServerProcess == null || Instance.serverSocket == null)
        {
            DebugLog.Error("Error starting Kinect logging: Cannot connect to server");
            return;
        }

        var message = new Command(SensorEnumType.Kinect, ActionEnumType.StartLogging, Instance.numPlayers == 1 ? LogEnumType.SingleUser : LogEnumType.MultiUser, Instance.Usernames);

        Instance.serverSocket.Send(JsonConvert.SerializeObject(message));
    }

    /// <summary>
    /// Send a message to the server to stop the Kinect logging.
    /// </summary>
    public static void StopKinectLogging()
    {
        if (Instance.multimodalServerProcess == null || Instance.serverSocket == null)
        {
            DebugLog.Error("Error stopping Kinect logging: Cannot connect to server");
            return;
        }
        var message = new Command(SensorEnumType.Kinect, ActionEnumType.StopLogging, Instance.numPlayers == 1 ? LogEnumType.SingleUser : LogEnumType.MultiUser, Instance.Usernames);

        Instance.serverSocket.Send(JsonConvert.SerializeObject(message));
        Instance.Usernames = "";
    }

    /// <summary>
    /// Send a message to the server to start the Open Face logging.
    /// </summary>
    public static void StartOpenFaceLogging()
    {
        DebugLog.Info("About to start open face logging");
        if (Instance.multimodalServerProcess == null || Instance.serverSocket == null)
        {
            DebugLog.Error("Error starting Open Face logging: Cannot connect to server");
            return;
        }

        var message = new Command(Instance.numPlayers == 1 ? SensorEnumType.OpenFace : SensorEnumType.OpenFaceMulti, ActionEnumType.StartLogging, Instance.numPlayers == 1 ? LogEnumType.SingleUser : LogEnumType.MultiUser, Instance.Usernames);

        Instance.serverSocket.Send(JsonConvert.SerializeObject(message));
    }

    /// <summary>
    /// Send a message to the server to stop the Open Face logging.
    /// </summary>
    public static void StopOpenFaceLogging()
    {
        DebugLog.Info("About to stop open face logging");
        if (Instance.multimodalServerProcess == null || Instance.serverSocket == null)
        {
            DebugLog.Error("Error stopping Open Face logging: Cannot connect to server");
            return;
        }

        var message = new Command(Instance.numPlayers == 1 ? SensorEnumType.OpenFace : SensorEnumType.OpenFaceMulti, ActionEnumType.StopLogging, Instance.numPlayers == 1 ? LogEnumType.SingleUser : LogEnumType.MultiUser, Instance.Usernames);

        Instance.serverSocket.Send(JsonConvert.SerializeObject(message));
        Instance.Usernames = "";
    }

    /// <summary>
    /// Send a message to the server to start the OpenSmile logging.
    /// </summary>
    public static void StartOpenSmileLogging()
    {
        DebugLog.Info("About to start OpenSmile logging");
        if (Instance.multimodalServerProcess == null || Instance.serverSocket == null)
        {
            DebugLog.Error("Error starting Open Face logging: Cannot connect to server");
            return;
        }

        var message = new Command(SensorEnumType.OpenSmile, ActionEnumType.StartLogging, LogEnumType.SingleUser, Instance.Usernames);

        Instance.serverSocket.Send(JsonConvert.SerializeObject(message));
    }

    /// <summary>
    /// Send a message to the server to stop the OpenSmile logging.
    /// </summary>
    public static void StopOpenSmileLogging()
    {
        DebugLog.Info("About to stop OpenSmile logging");
        if (Instance.multimodalServerProcess == null || Instance.serverSocket == null)
        {
            DebugLog.Error("Error stopping Open Face logging: Cannot connect to server");
            return;
        }

        var message = new Command(SensorEnumType.OpenSmile, ActionEnumType.StopLogging, LogEnumType.SingleUser, Instance.Usernames);

        Instance.serverSocket.Send(JsonConvert.SerializeObject(message));
        Instance.Usernames = "";
    }

    //public static void StartTraceLog()
    //{
    //    DebugLog.Info("About to start tracelog");
    //    Instance.logStartTime = GameTime.time;
    //    if (!string.IsNullOrEmpty(Instance.traceLogBackupDrive))
    //    {
    //        var fileName = String.Format("{0}.{1}", Instance.Usernames + "_GameplayAndEyeGaze_" + DateTime.Now.ToString("mm-dd-yyyy--HH-mm-ss"), SerializerCsv.Instance.FilenameExtension);

    //        var traceLogFile = Path.Combine(Instance.traceLogBackupDrive, fileName);

    //        TraceLog.Open(Guid.NewGuid().ToString(), new FileLogger(traceLogFile, SerializerCsv.Instance));
    //    }

    //    TraceLog.Player(TraceLog.Action.Started, "GameStart",
    //        "time", GameTime.time,
    //        "subject id", Instance.Usernames);
    //}

    //public static void StopTraceLog()
    //{
    //    DebugLog.Info("About to stop tracelog");
    //    TraceLog.Player(TraceLog.Action.Ended, "GameEnd",
    //        "time", GameTime.time,
    //        "subject id", Instance.Usernames,
    //        "start", Instance.logStartTime,
    //        "duration", GameTime.time - Instance.logStartTime);

    //    _ = TraceLog.CloseAsync();
    //}

    /// <summary>
    /// Attempt to connect to the multimodal server.
    /// </summary>
    public static void ConnectToServer()
    {
        if (Instance.multimodalServerProcess == null)
        {
            DebugLog.Error("Cannot connect to server, process not started");

            return;
        }

        if (Instance.serverSocket != null)
        {
            DisconnectFromServer();
        }

        Instance.serverSocket = new WebSocket(string.Format("ws://{0}:{1}", Instance.ip, Instance.port));

        Instance.serverSocket.OnMessage += OnServerMessage;
        Instance.serverSocket.OnOpen += (s, e) => DebugLog.Info("Client connected to multimodal server.");
        Instance.serverSocket.OnError += (s, e) => DebugLog.Error("Error with multimodal server: {0}", e.Message);

        Instance.serverSocket.Connect();
    }

    /// <summary>
    /// Disconnect from the multimodal server.
    /// </summary>
    public static void DisconnectFromServer()
    {
        DebugLog.Info("Client force stop multimodal server");
        Instance.serverSocket?.Close();
        Instance.serverSocket = null;
    }

    private static void OnServerMessage(object sender, MessageEventArgs e)
    {
        DebugLog.Info("Got message from server:" + e.Data);
        var sensor = JsonConvert.DeserializeObject<TrackingCheck>(e.Data);

        // switch (sensor.SensorType)
        // {
        //     case SensorEnumType.Kinect:
        //         Instance.isKinectTracking = sensor.IsTracking;
        //         break;
        //     case SensorEnumType.OpenFace:
        //     case SensorEnumType.OpenFaceMulti:
        //         Instance.isOpenFaceTracking = sensor.IsTracking;
        //         break;
        // }
    }
}

#endif
